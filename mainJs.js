var app = angular.module("addrBook", []);
app.controller("myCtrl", function($scope) {
  $scope.info = [
    { name: "prince", tel: "9066393213" },
    { name: "raj", tel: "9066393214" },
    { name: "manish", tel: "9066393215" }
  ];

  $scope.addMe = function() {
    return {
      name: $scope.newName,
      tel: $scope.newTel
    };
  };
  $scope.addItem = function() {
    for (var i = 0; i < $scope.info.length; i++) {
      if (
        $scope.info[i].name == $scope.addMe().name ||
        $scope.info[i].tel == $scope.addMe().tel
      ) {
        alert("name or telphone number was repeated");
        return false;
      }
    }
    $scope.info.push($scope.addMe());
  };

  $scope.removeItem = function() {
    $scope.info.splice(this.$index, 1);
  };
});
